# Flag

A command line parsing library inspired by the similarly named package in the
Go standard library (https://golang.org/pkg/flag/) as well as
https://github.com/spf13/pflag but for Nim.

## Example

*NOTE:* The interface is rapidly changing while trying to make it more Nim-like.
This may become out of date.

The current best practice is to use pointers and references.

```nim
import os
import flag

const
  VERSION = "0.0.0"
  USAGE = """Usage: flag [OPTIONS]"""

var
  name = "World"
  showHelp: bool
  showVersion: bool
  countTo = 10
  radius = 10.0

let fs = newFlagSet()
fs.add(name.addr, "n", "name", "The name")
fs.add(showHelp.addr, "h", "help", "Show the help")
fs.add(showVersion.addr, "v", "version", "Show the version")
fs.add(radius.addr, "r", "radius", "The radius of a circle")

# Where all the magic happens
fs.parse(commandLineParams())

if showVersion:
  quit(VERSION)
if showHelp:
  echo USAGE
  quit(flags.optionStr())

# Greet
echo "Hello, " & name & "!"

# Count
for i in (0..countTo):
  echo $i

# Calculate
echo "Circle circumference is " & $(radius * 2 * 3.1415)
```

## Documentation
See full documentation [here](https://antizealot1337.gitlab.io/flag/)

## License
Licensed under the terms of the MIT license. See LICENSE file for more
information.
