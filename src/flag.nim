# Copyright (c) 2018 Chris
#
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import os
import strutils

type
  FlagVal* = tuple[         ## An inteface for flag values.
    parse: proc(s: string), ## Parse the value from a string
    get: proc(): string,    ## Get the current value as a string
  ]
  FlagType = enum ## The different flag types.
    Switch,       ## Flag acts like a toggle switch.
    Value,         ## Flag will parse a value from the FlagVal.
  Flag = ref object ## Represents a flag that may exist.
    short: string
    long: string
    help: string
    kind: FlagType
    val: FlagVal
  FlagSet* = ref object ## Contains the flags and (after a parse) the remaining args.
    flags: seq[Flag]
    args*: seq[string]
    compat: bool

converter toFlagVal*(val: ptr bool or ref bool): FlagVal =
  ## Convert a pointer or reference to a bool to a FlagVal.
  result.parse = proc(s: string) {.closure.} =
    if s == "":
      val[] = not val[]
    else:
      val[] = parseBool(s)
  result.get = proc(): string {.closure.} = $(val[])

converter toFlagVal*(val: ptr int or ref int): FlagVal =
  ## Convert a pointer or reference to an int to a FlagVal.
  result.parse = proc(s: string) {.closure.} = val[] = parseInt(s)
  result.get = proc(): string {.closure.} = $(val[])

converter toFlagVal*(val: ptr uint or ref uint): FlagVal =
  ## Convert a pointer or reference to an int to a FlagVal.
  result.parse = proc(s: string) {.closure.} = val[] = parseUint(s)
  result.get = proc(): string {.closure.} = $(val[])

converter toFlagVal*(val: ptr float or ref float): FlagVal =
  ## Convert a pointer or reference to a float to a FlagVal.
  result.parse = proc (s: string) {.closure.} = val[] = parseFloat(s)
  result.get = proc(): string {.closure.} = $(val[])

converter toFlagVal*(val: ptr string or ref string): FlagVal =
  ## Convert a pointer or reference to a string to a FlagVal.
  result.parse = proc(s: string) {.closure.} = val[] = s
  result.get = proc(): string = val[]

proc seqFlagVal*[T](val: ptr seq[T] or ref seq[T], parse: proc(s: string): T): FlagVal =
  ## Convert a pointer or reference to a seq to a FlagVal.
  result.parse = proc(s: string) {.closure.} = (val[]).add(parse(s))
  result.get = proc(): string {.closure.} = $(val[])

proc `$`(self: Flag): string =
  ## Convert a Flag to its help/usage representation. The output will change
  ## depending on if a short value is provided (not empty) or not.
  result =
    if self.short == "":
      "\t--$#\t\t$#" % [self.long, self.help]
    else:
      "\t-$#, --$#\t\t$#" % [self.short, self.long, self.help]

proc set(self: Flag, value: string) =
  ## Set the value of the flag from the provided string.
  self.val.parse(value)

proc newFlagSet*(compat = true): FlagSet =
  ## Create a new FlagSet. By provided false to this proc it will disable
  ## parseopt compatibility.
  result = FlagSet(compat: compat)

proc add*(self: FlagSet, val: FlagVal, short, long, help: string, kind = Value) =
  ## This adds a flag to the Flag set with the FlagVal and other various
  ## information. It is really only useful when creating a specialized flag
  ## type that needs to act as a Switch (i.e. an accumulator that doesn't
  ## accept data). It's generally advised to use the other add procs in most
  ## cases.
  self.flags.add(Flag(short: short, long: long, help: help, kind: kind, val: val))

proc add*[T](self: FlagSet, val: ptr T or ref T, short, long, help: string) =
  ## Add a flag to the FlagSet with the given information.
  when T is bool:
    self.add(val.toFlagVal, short, long, help, Switch)
  else:
    self.add(val.toFlagVal, short, long, help, Value)

proc add*[T](self: FlagSet, val: ptr T or ref T, flag, help: string) {.inline.} =
  ## This is a convenience proc to create a flag that only responds to long
  ## options (i.e. "--"). This is the same as calling add with an empty string
  ## for short.
  self.add(val, "", flag, help)

proc addBool*(self: FlagSet, val: ptr bool or ref bool, long, help: string) {.inline, deprecated: "use add instead".} =
  ## Create a bool flag with only a long option ("--") for the FlagSet. This is
  ## the same as calling addBool and supplying an empty string for short.
  ##
  ## **DEPRECATED:** Use add instead
  self.add(val, long, help)

proc addBool*(self: FlagSet, val: ptr bool or ref bool, short, long, help: string) {.inline, deprecated: "use add instead".} =
  ## Create a bool flag for the FlagSet.
  ##
  ## **DEPRECATED:** Use add instead
  self.add(val, short, long, help)

proc addInt*(self: FlagSet, val: ptr int or ref int, long, help: string) {.inline, deprecated: "use add instead".} =
  ## Create an int flag with only a long option ("--") for the FlagSet. This is
  ## the same as calling addInt and supplying an empty string for short.
  ##
  ## **DEPRECATED:** Use add instead
  self.add(val, long, help)

proc addInt*(self: FlagSet, val: ptr int or ref int, short, long, help: string) {.inline, deprecated: "use add instead".} =
  ## Create an int flag for the FlagSet.
  ##
  ## **DEPRECATED:** Use add instead
  self.add(val, short, long, help)

proc addFloat*(self: FlagSet, val: ptr float or ref float, long, help: string) {.inline, deprecated: "use add instead".} =
  ## Create an float flag with only a long option ("--") for the FlagSet. This
  ## is the same as calling addFloat and supplying and empty string for short.
  ##
  ## **DEPRECATED:** Use add instead
  self.add(val, long, help)

proc addFloat*(self: FlagSet, val: ptr float or ref float, short, long, help: string) {.inline, deprecated: "use add instead".} =
  ## Create a float flag for the FlagSet.
  ##
  ## **DEPRECATED:** Use add instead
  self.add(val, short, long, help)

proc addString*(self: FlagSet, val: ptr string or ref string, long, help: string) {.inline, deprecated: "use add instead".} =
  ## Create an string flag with only a long option ("--") for the FlagSet. This
  ## is the same as calling addString and supplying and empty string for short.
  ##
  ## **DEPRECATED:** Use add instead
  self.add(val, long, help)

proc addString*(self: FlagSet, val: ptr string or ref string, short, long, help: string) {.inline, deprecated: "use add instead".} =
  ## Create a string flag for the FlagSet.
  ##
  ## **DEPRECATED:** Use add instead
  self.add(val, short, long, help)

proc findFlag(self: FlagSet, name: string): Flag =
  ## Find a Flag given a name.
  for flag in self.flags:
    if flag.short == name or flag.long == name:
      return flag

proc getDataIdx(arg: string, compat: bool): int =
  ## Get the data index. This proc enables compatibility with the standard
  ## library's `parseopt`.
  result = arg.find("=")
  if result == -1 and compat:
    result = arg.find(":")

proc parseArg(arg: string, compat: bool): tuple[name: string, data: string] =
  ## Parse the argument to get the name an possibly data
  if arg.startsWith("--"):
    # Get the possible index for the data
    let dataIdx = getDataIdx(arg, compat)

    # Check if there is data associated with the arg
    if dataIdx == -1:
      # This is only a flag
      result.name = arg[2..^1]
    else:
      # Set the name
      result.name = arg[2..dataIdx-1]

      # Set the data
      result.data = arg[dataIdx+1..^1]
  elif arg.startsWith("-"):
    # Make sure there's more to the flag that just the switch
    if arg.len() < 2:
      raise newException(ValueError, "Invalid flag \"-\"")

    # Set the name
    result.name = arg[1..1]

    # Check if there is data associated with the arg
    if arg.len() > 2:
      result.data = arg[2..^1]
  else: discard

proc parse*(self: FlagSet, args: seq[string]) =
  ## Parse the arguments looking for the flags.

  # A flag here means the next agument will be consumed by this
  # flag
  var consumer: Flag = nil

  # Loop through the args
  for i in (0..args.len-1):
    # The current arg
    let arg = args[i]

    if consumer == nil:
      # Parse the arg
      let (name, data) = parseArg(arg, self.compat)

      # Check if this is an argument
      if name == "":
        # Add the args to the flag set
        self.args.add(args[i..^1])

        # End the loop
        return

      # Find the flag
      consumer = self.findFlag(name)

      # Make sure the flag was found
      if consumer == nil:
        raise newException(ValueError, "No flag named \"$#\"" % [name])

      # Check if the flag is a bool flag
      if consumer.kind == Switch:
        # Set the data
        consumer.set(data)

        # Clear the consumer
        consumer = nil
        continue

      # Check if there was data associated with the arg
      if data != "":
        # Set the consumer
        consumer.set(data)

        # Clear the consumer
        consumer = nil
    else:
      # Set the consumer
      consumer.set(arg)

      # Clear the consumer
      consumer = nil

template parse*(self: FlagSet) =
  ## Parse the flags from the command line. This is the same as calling
  ## parse(fs, commandLineParams()).
  parse(self, commandLineParams())

proc optionStr*(self: FlagSet): string =
  ## Return a string with all the flags.
  result = ""

  ## Loop through the flags
  for flag in self.flags:
    result.add($flag & "\n")
