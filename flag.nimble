# Package

version       = "0.7.0"
author        = "Christopher Pridgen"
description   = "POSIX-like flag parsing inspired by Go's flag package"
license       = "MIT"
srcDir        = "src"


# Dependencies

requires "nim >= 0.19.0"
