# This is just an example to get you started. You may wish to put all of your
# tests into a single file, or separate them into multiple `test1`, `test2`
# etc. files (better names are recommended, just make sure the name starts with
# the letter 't').
#
# To run these tests, simply execute `nimble test`.

import strutils
import unittest

import flag

suite "flags":
  test "user flag":
    var
      sval = ""
      fv: FlagVal = (
        parse: proc(s: string) {.closure.} = sval = s,
        get: proc(): string {.closure.} = sval,
      )
    fv.parse("a")
    check(sval == "a")

suite "flag set":
  test "can create flag set":
    let fs = newFlagSet()
    check(fs != nil)

  test "add flags":
    var
      b: bool
      i: int
      f: float
      s: string
      ss: seq[string]
    let fs = newFlagSet()
    fs.add(b.addr, "b", "bool", "")
    fs.add(i.addr, "i", "int", "")
    fs.add(f.addr, "f", "float", "")
    fs.add(s.addr, "string", "")
    fs.add(seqFlagVal(ss.addr, proc(s: string): string = s), "x", "seq", "")

  test "parse short flags":
    var
      b: bool
      i: int
      f: float
      s: string
      ss: seq[string]
    let
      fs = newFlagSet()
    fs.add(b.addr, "b", "bool", "")
    fs.add(i.addr, "i", "int", "")
    fs.add(f.addr, "f", "float", "")
    fs.add(s.addr, "s", "string", "")
    fs.add(seqFlagVal(ss.addr, proc(s: string): string = s), "x", "seq", "")
    fs.parse(@["-b", "-i", "1", "-f", "2.0", "-s", "c", "-x", "a", "-x", "b"])
    check(b)
    check(i == 1)
    check(f == 2.0)
    check(s == "c")
    check(ss.len == 2)
    check(ss[0] == "a")
    check(ss[1] == "b")

  test "parse short flags with values":
    var
      b: bool
      i: int
      f: float
      s: string
      ss: seq[string]
    let
      fs = newFlagSet()
    fs.add(b.addr, "b", "bool", "")
    fs.add(i.addr, "i", "int", "")
    fs.add(f.addr, "f", "float", "")
    fs.add(s.addr, "s", "string", "")
    fs.add(seqFlagVal(ss.addr, proc(s: string): string = s), "x", "seq", "")
    fs.parse(@["-b", "-i1", "-f2.0", "-sc", "-xa", "-xb"])
    check(b)
    check(i == 1)
    check(f == 2.0)
    check(s == "c")
    check(ss.len == 2)
    check(ss[0] == "a")
    check(ss[1] == "b")

  test "parse long flags":
    var
      b: bool
      i: int
      f: float
      s: string
      ss: seq[string]
    let
      fs = newFlagSet()
    fs.add(b.addr, "b", "bool", "")
    fs.add(i.addr, "i", "int", "")
    fs.add(f.addr, "f", "float", "")
    fs.add(s.addr, "s", "string", "")
    fs.add(seqFlagVal(ss.addr, proc(s: string): string = s), "x", "seq", "")
    fs.parse(@["--bool", "--int", "1", "--float", "2.0", "--string", "c",
      "--seq", "a", "--seq", "b"])
    check(b)
    check(i == 1)
    check(f == 2.0)
    check(s == "c")
    check(ss.len == 2)
    check(ss[0] == "a")
    check(ss[1] == "b")

  test "parse long flags with data":
    var
      b: bool
      i: int
      f: float
      s: string
      ss: seq[string]
    let
      fs = newFlagSet()
    fs.add(b.addr, "b", "bool", "")
    fs.add(i.addr, "i", "int", "")
    fs.add(f.addr, "f", "float", "")
    fs.add(s.addr, "s", "string", "")
    fs.add(seqFlagVal(ss.addr, proc(s: string): string = s), "x", "seq", "")
    fs.parse(@["--bool", "--int=1", "--float=2.0", "--string=c", "--seq=a",
      "--seq=b"])
    check(b)
    check(i == 1)
    check(f == 2.0)
    check(s == "c")
    check(ss.len == 2)
    check(ss[0] == "a")
    check(ss[1] == "b")

  test "parse long flags with data(compat)":
    var
      b: bool
      i: int
      f: float
      s: string
      ss: seq[string]
    let
      fs = newFlagSet()
    fs.add(b.addr, "b", "bool", "")
    fs.add(i.addr, "i", "int", "")
    fs.add(f.addr, "f", "float", "")
    fs.add(s.addr, "s", "string", "")
    fs.add(seqFlagVal(ss.addr, proc(s: string): string = s), "x", "seq", "")
    fs.parse(@["--bool", "--int:1", "--float:2.0", "--string:c", "--seq:a",
      "--seq:b"])
    check(b)
    check(i == 1)
    check(f == 2.0)
    check(s == "c")
    check(ss.len == 2)
    check(ss[0] == "a")
    check(ss[1] == "b")

  test "args left over":
    var
      b: bool
      i: int
    let
      fs = newFlagSet()
    fs.add(b.addr, "b", "bool", "")
    fs.add(i.addr, "i", "int", "")
    fs.parse(@["-b", "arg", "-i"])
    check(fs.args.len() == 2)
    check(fs.args[0] == "arg")
    check(fs.args[1] == "-i")

  test "options str":
    const
      expected = """\t-b, --bool\t\tA boolean flag.
\t-i, --int\t\tAn integer flag.
""".replace("\\t", "\t")
    var
      b: bool
      i: int
    let
      fs = newFlagSet()
    fs.add(b.addr, "b", "bool", "A boolean flag.")
    fs.add(i.addr, "i", "int", "An integer flag.")
    check(fs.optionStr() == expected)
